## GCTV - Google calendar & task viewer

GCTV is an app for google calendar and tasks letting the user to create, delete and view schedule elements.

### Installation guideApp requires python with version at least 3.0.0

1. Place your credentials.json file in the config folder. [Here is how to get credentials.json file ](https://developers.google.com/calendar/quickstart/python) and click Enable the Google Calendar API.
2. Run set_pickles.py to and approve access to the calendar and tasks for the app. Make sure your *.pickle files will appear in the app folder.
3. Being in app directory Run: sudo docker build -t tacal .

### Usage (for docker image)

1. View items 
   ```
   docker run GCTV 
   ```
    ![img](/img/GCTV_main.png)
2. Create an event
    ```
    docker run GCTV -c E:"Title":12:00:14:00
    ```
3. Create an task
    ```
    docker run GCTV -c T:"some title":"description"
    ```
4. Delete an element * id_ele - is Id field at first image
    ```
    docker run GCTV -d id_ele
    ```
5. Print more information about element
    ```
    docker run GCTV -i id_ele
    ```