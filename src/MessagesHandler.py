import json
from os import path


# singleton pattern
class MessagesHandler:
    __instance = {}
    messages = {}

    def __init__(self):
        self.__dict__ = self.__instance

        script_dir = path.dirname(__file__)
        config_file_name = "../config/messages.json"
        with open(path.join(script_dir, config_file_name)) as messages_file:
            self.messages = json.load(messages_file)

    def get_message(self, mes_type, code):
        return self.messages.get(mes_type, {code: ''}).get(code, '')
