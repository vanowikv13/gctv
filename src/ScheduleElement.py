import datetime
import json
import os.path
from abc import ABC, abstractmethod

script_dir = os.path.dirname(__file__)
schedule_config_file_name = "../config/schedule_elements_type.json"

with open(os.path.join(script_dir, schedule_config_file_name)) as schedule_ele_file:
    elements_type = json.load(schedule_ele_file)


# base class
class ScheduleElement(ABC):
    def __init__(self, element, ele_type, id_list):
        self.type = ele_type
        self.element = element
        self.id_list = id_list
        self.start = ""
        self.end = ""

    @abstractmethod
    def getPrintableElement(self, index):
        pass

    def getPrintableDate(self):
        if len(self.start) > 1 and len(self.end) > 1:
            return datetime.datetime.fromisoformat(self.start).strftime("%H:%M") + "-" + datetime.datetime.fromisoformat(self.end).strftime("%H:%M")
        return ''

    @staticmethod
    def getIsoFormatDate(event, type_date):
        date_time = event.get(type_date, {'dateTime': ''}).get('dateTime', '').replace('Z', '')
        if len(date_time) > 1:
            return date_time
        return ''

    @staticmethod
    def stringCutter(string, width):
        if len(string) <= width:
            return string
        return string[:len(string) - (len(string) - width)]


class Task(ScheduleElement):
    def __init__(self, element, id_list='@default'):
        super().__init__(element, 'task', id_list)
        self.title = element.get('title', '')
        self.status = elements_type.get('taskStatusResponses', '').get(element.get('status', ''))

    def getPrintableElement(self, index):
        return [
            (' 0' + str(index)) if index < 10 else (' ' + str(index)),
            'T',
            self.status,
            self.title
        ]

    def getFullPrintableElement(self, index):
        to_print = self.getPrintableElement(index)
        to_print[-1] += ' / ' + self.element.get('notes', '')
        return to_print


class Event(ScheduleElement):
    def __init__(self, element, id_list='@primary'):
        super().__init__(element, 'event', id_list)
        self.title = element.get('summary', '')

        self.start = self.getIsoFormatDate(element, 'start')
        self.end = self.getIsoFormatDate(element, 'end')
        self.printableData = self.getPrintableDate()

    # operator < overload for sorting events
    def __lt__(self, other):
        return not (self.start.strftime("%H") > other.start.strftime("%H"))

    def getPrintableElement(self, index):
        return [
            (' 0' + str(index)) if index < 10 else (' ' + str(index)),
            'E',
            self.printableData if len(self.printableData) > 0 else '---',
            self.title
        ]

    def getFullPrintableElement(self, index):
        to_print = self.getPrintableElement(index)
        to_print[-1] += ' / ' + self.element.get('description', '')
        return to_print
