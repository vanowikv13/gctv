import datetime
import pickle
import json
from os import path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from abc import ABC, abstractmethod

# for error and warning messages
from src.MessagesHandler import MessagesHandler

mes_handler = MessagesHandler()

script_dir = path.dirname(__file__)
config_file_name = "../config/config.json"

with open(path.join(script_dir, config_file_name)) as config_file:
    config = json.load(config_file)


class ConnectionHandler(ABC):
    def __init__(self, scope, pickle_name):
        self.SCOPE = scope

        # getting credentials for a specific service
        self.cred = self.getCredForService(pickle_name, scope)

        # getting us correct time for queries to get events and tasks
        self.start_time = datetime.datetime.now().replace(hour=0, minute=0, second=0).isoformat() + 'Z'
        self.end_time = (datetime.datetime.now()
                         + datetime.timedelta(days=1)).replace(hour=0, minute=0, second=0).isoformat() + 'Z'

    @abstractmethod
    def build_service(self):
        pass

    @abstractmethod
    def deleteElement(self, element, list_):
        pass

    @abstractmethod
    def createElement(self, element, list):
        pass

    @staticmethod
    def getCredForService(pickle_name, scope):
        cred = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.event
        if path.exists(pickle_name):
            with open(pickle_name, 'rb') as token:
                cred = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not cred or not cred.valid:
            if cred and cred.expired and cred.refresh_token:
                cred.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    path.join(script_dir, config.get('pathToCredentialsFile', '')), scope)
                cred = flow.run_local_server(port=0, host='127.0.0.1')
            # Save the credentials for the next run
            with open(pickle_name, 'wb') as token:
                pickle.dump(cred, token)
        return cred


class CalendarRequestsHandler(ConnectionHandler):
    def __init__(self):
        super().__init__(config.get('CALENDAR_SCOPES', []), path.join(script_dir, config.get('pathToCalendarPickle', '')))
        self.service = self.build_service()

    def build_service(self):
        return build('calendar', 'v3', credentials=self.cred)

    def getCalendars(self):
        calendar_result = self.service.calendarList().list().execute()
        return calendar_result.get('items', [])

    def getCalendarsItems(self):
        calendars = self.getCalendars()
        events_res = {}

        if not calendars:
            print(mes_handler.get_message('info', 'noCalendarsFound'))
        else:
            for calendar in calendars:
                calendar_id = calendar.get('id', '')
                events_res[calendar_id] = []
                events_result = self.service.events().list(
                    calendarId=calendar_id, timeMin=self.start_time, timeMax=self.end_time, singleEvents=True,
                    orderBy='startTime', fields='items(summary,id,description,start/dateTime,end/dateTime)').execute()
                events_res[calendar_id] = events_result.get('items', [])
        return events_res

    def deleteElement(self, event, calendar='@default'):
        print(event)
        self.service.events().delete(calendarId=calendar, eventId=event.get('id', '')).execute()

    def createElement(self, event, calendar='primary'):
        self.service.events().insert(calendarId=calendar, body=event).execute()


class TaskRequestsHandler(ConnectionHandler):
    def __init__(self):
        super().__init__(config.get('TASKS_SCOPES', ''), path.join(script_dir, config.get('pathToTasksPickle', '')))
        self.service = self.build_service()

    def build_service(self):
        return build('tasks', 'v1', credentials=self.cred)

    def getTasksListsName(self):
        tasks_result = self.service.tasklists().list().execute()
        return tasks_result.get('items', [])

    def getTasksItems(self):
        tasks_lists = self.getTasksListsName()
        tasks_elements = {}

        if not tasks_lists:
            print(mes_handler.get_message('info', 'noTasksListsFound'))
        else:
            for task in tasks_lists:
                task_list_id = task.get('id', '')
                tasks_elements[task_list_id] = None
                tasks = self.service.tasks().list(tasklist=task_list_id, dueMin=self.start_time,
                                                  dueMax=self.end_time, showHidden=True,
                                                  fields='items(id,title,notes,due,status)').execute()
                tasks_elements[task_list_id] = tasks.get('items', [])
        return tasks_elements

    def changeTaskStatus(self, task, tasklist='@default'):
        if task.get('status', '') == 'completed':
            task['status'] = 'to do'
        else:
            task['status'] = 'completed'

        task_result = self.service.tasks().update(tasklist=tasklist, task=task.get('id', ''), body=task).execute()
        return task_result

    def deleteElement(self, task, tasklist='@default'):
        print(task)
        self.service.tasks().delete(tasklist=tasklist, task=task.get('id', '')).execute()

    def createElement(self, task, tasklist='@default'):
        self.service.tasks().insert(tasklist='@default', body=task).execute()