import datetime

from src.MessagesHandler import MessagesHandler
from src.ScheduleElement import Task, Event, ScheduleElement
from src.ConnectionHandler import CalendarRequestsHandler, TaskRequestsHandler

# Doc: https://github.com/willmcgugan/rich
from rich.console import Console
from rich.table import Table


class CalTaskService:
    def __init__(self):
        self.cal_events = []
        self.tasks = []

        self.calendar_service = CalendarRequestsHandler()
        self.tasks_service = TaskRequestsHandler()

        # getting elements from APIs
        self.__cal_task_service()
        self.sch_size = len(self.tasks) + len(self.cal_events)

        self.mes_handler = MessagesHandler()

        self.__set_table()
        self.console = Console()

    def __cal_task_service(self):
        tasks_ = self.tasks_service.getTasksItems()
        events = self.calendar_service.getCalendarsItems()

        for id_list in events:
            for event in events[id_list]:
                self.cal_events.append(Event(event, id_list))
        for id_list in tasks_:
            for task in tasks_[id_list]:
                self.tasks.append(Task(task, id_list))

    def print_menu(self):
        index = 0
        for element in self.cal_events + self.tasks:
            self.__add_element_to_table(element.getPrintableElement(index))
            index += 1
        self.__print_elements_table()

    def get_info_of_element(self, index):
        self.__check_if_index_is_correct(index)
        sch_element = self.__get_element_of_a_user_index(index)

        self.__set_table()

        full_element = sch_element.getFullPrintableElement(index)
        self.__add_element_to_table(full_element)
        self.__print_elements_table()

    def __add_element_to_table(self, print_ele):
        self.table.add_row(print_ele[0], print_ele[1], print_ele[2], print_ele[3])

    def __print_elements_table(self):
        self.console.print(self.table)

    def __set_table(self):
        self.table = Table(title='[bold red]GCTV[/bold red]')
        self.table.add_column("Id", justify="left", style="gold1", no_wrap=True)
        self.table.add_column(" T ", justify="center", style="green")
        self.table.add_column("Date/Status", justify="left", style="deep_sky_blue1")
        self.table.add_column("Title/Description", justify="left", style="hot_pink")

    def delete_element(self, index):
        self.__check_if_index_is_correct(index)
        sch_element = self.__get_element_of_a_user_index(index)

        if sch_element.type == 'event':
            self.__delete(self.calendar_service, sch_element)
        elif sch_element.type == 'task':
            self.__delete(self.tasks_service, sch_element)
        else:
            self.mes_handler.get_message('info', 'couldNotDeleteElement')

    def __get_element_of_a_user_index(self, index):
        if index > len(self.cal_events) - 1:
            return self.tasks[index - len(self.cal_events)]
        return self.cal_events[index]

    def __delete(self, service, sch_element):
        service.deleteElement(sch_element.element, sch_element.id_list)
        if sch_element in self.cal_events:
            self.cal_events.remove(sch_element)
        elif sch_element in self.tasks:
            self.tasks.remove(sch_element)

    def create_element(self, arg_data, ele_type):
        index = 2
        title = get_value_from_separated_string(arg_data, ':', index)
        index += len(title) + 1
        if ele_type == 'E':
            # getting date
            s_hh = get_value_from_separated_string(arg_data, ':', index)
            index += len(s_hh) + 1
            s_mm = get_value_from_separated_string(arg_data, ':', index)
            index += len(s_mm) + 1
            e_hh = get_value_from_separated_string(arg_data, ':', index)
            index += len(e_hh) + 1
            e_mm = get_value_from_separated_string(arg_data, ':', index)

            event = get_event_to_insert(title, s_hh, s_mm, e_hh, e_mm)
            self.calendar_service.createElement(event)
            self.cal_events.append(Event(event))
            self.cal_events.sort()
        elif ele_type == 'T':
            note = get_value_from_separated_string(arg_data, ':', index)
            task = get_task_to_insert(title, note)
            self.tasks_service.createElement(task)
            self.tasks.append(Task(task))
        else:
            raise Exception(self.mes_handler.get_message('error', 'wrongCommandUsage'))

    def mark_element(self, index):
        self.__check_if_index_is_correct(index)
        sch_element = self.tasks[index - len(self.cal_events)]
        result = self.tasks_service.changeTaskStatus(sch_element.element, sch_element.id_list)
        self.tasks[index - len(self.cal_events)] = Task(result, sch_element.id_list)

    def __check_if_index_is_correct(self, index):
        if index >= self.sch_size or index < 0:
            raise Exception(self.mes_handler.get_message('error', 'wrongIdNumber'))


# ------------- functions that using a class and others ---------------------------------
def get_string_time(x, length):
    x = str(x)
    for i in range(length - len(x)):
        x = '0' + x
    return x


def get_event_to_insert(title, s_hh, s_mm, e_hh, e_mm):
    now = datetime.datetime.now()
    now_time = get_string_time(now.year, 4) + '-' + get_string_time(now.month, 2) + '-' + get_string_time(now.day,
                                                                                                          2) + 'T'
    start_time = now_time + get_string_time(s_hh, 2) + ':' + get_string_time(s_mm, 2) + ':00+02:00'
    end_time = now_time + get_string_time(e_hh, 2) + ':' + get_string_time(e_mm, 2) + ':00+02:00'

    return {
        'summary': title,
        'description': '',
        'start': {
            'dateTime': start_time,
        },
        'end': {
            'dateTime': end_time,
        },
    }


def get_task_to_insert(title, note):
    now = datetime.datetime.now()
    now_time = get_string_time(now.year, 4) + '-' + get_string_time(now.month, 2) + '-' + get_string_time(now.day,
                                                                                                          2) + 'T00:00:00.000Z'
    return {
        'title': title,
        'notes': note,
        'due': now_time,
        'status': 'needsAction'
    }


# i should be on character right after separation sigh
def get_value_from_separated_string(str_, separation_sign, i=0):
    x = ""
    while i < len(str_):
        if str_[i] == separation_sign:
            break
        x += str_[i]
        i += 1
    return x
