#!/usr/bin/env python

# for setting pickles
from src.ConnectionHandler import CalendarRequestsHandler, TaskRequestsHandler

calendar_service = CalendarRequestsHandler()
task_service = TaskRequestsHandler()
