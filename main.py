#!/usr/bin/env python
import getopt
import sys

from src.CalTaskService import CalTaskService
from src.MessagesHandler import MessagesHandler


def check_service(service):
    if service is None:
        return CalTaskService()
    return service


def main(argv):
    mes_handler = MessagesHandler()
    cal_task_service = None

    try:
        opts, args = getopt.getopt(argv, 'd:c:m:i:', ['help', 'delete=', 'create=', 'mark=', 'info='])
        for opt, arg in opts:
            if opt == '--help':
                print(mes_handler.get_message('info', 'help'))
                sys.exit(0)
            elif opt in ('-i', '--info'):
                cal_task_service = check_service(cal_task_service)
                cal_task_service.get_info_of_element(int(arg))
                sys.exit(0)
            elif opt in ('-d', '--delete'):
                cal_task_service = check_service(cal_task_service)
                cal_task_service.delete_element(int(arg))
            elif opt in ('-c', '--create'):
                cal_task_service = check_service(cal_task_service)
                cal_task_service.create_element(arg, arg[0])
            elif opt in ('-m', '--mark'):
                cal_task_service = check_service(cal_task_service)
                cal_task_service.mark_element(int(arg))
    except getopt.GetoptError:
        print(mes_handler.get_message('error', 'wrongCommandUsage'))
        print(mes_handler.get_message('info', 'help'))
        sys.exit(-1)
    except Exception as ex:
        print(ex)
        print(mes_handler.get_message('info', 'help'))
        sys.exit(-1)

    cal_task_service = check_service(cal_task_service)
    cal_task_service.print_menu()


if __name__ == '__main__':
    main(sys.argv[1:])
