FROM python:3
WORKDIR /
COPY . /
RUN pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib rich
ENTRYPOINT ["python", "main.py"]
